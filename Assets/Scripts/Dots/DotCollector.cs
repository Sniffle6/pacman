﻿using UnityEngine;
using UnityEngine.Tilemaps;

public class DotCollector : MonoBehaviour
{
    [SerializeField]
    private Tilemap _tilemap;
    private BoundsInt _bounds;

    [SerializeField]
    private FloatObject dotsCollected;
    [SerializeField]
    private float _totalDots;

    private Movement _movement;

    private void Awake()
    {
        _movement = GetComponent<Movement>();
        _movement.onBeforeSetDirection += CollectDot;

        _bounds = _tilemap.cellBounds;
    }

    private void Start()
    {

        foreach (var pos in _tilemap.cellBounds.allPositionsWithin)
        {
            Vector3Int localPlace = new Vector3Int(pos.x, pos.y, pos.z);
            Vector3 place = _tilemap.CellToWorld(localPlace);
            if (_tilemap.HasTile(localPlace))
            {
                _totalDots++;
            }
        }
    }

    public void CollectDot(Vector2 direction)
    {
        var pos = Vector3Int.FloorToInt(transform.position);
        if (_tilemap.GetTile(pos)){
            dotsCollected.Value += 1;
            _tilemap.SetTile(pos, null);
        }
    }

    private void OnApplicationQuit()
    {
        dotsCollected.Value = 0;
    }
}
