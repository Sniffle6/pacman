﻿using UnityEngine;

[CreateAssetMenu(fileName ="New Float", menuName = "Float Object")]
public class FloatObject : ScriptableObject
{
    public float Value;
}