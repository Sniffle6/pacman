﻿public interface IDie
{
    Entity Parent { get; set; }
    void Die();
    void Respawn();
}


