﻿using System.Collections;
using UnityEngine;

public class Pacman : Entity
{
    
    protected override void Awake()
    {
        base.Awake();
        Target = new Vector2(-12.5f, 14f);
        movement.Input = new PacmanInput();
        movement.Input.OnEnable();

        //StartCoroutine(test());
    }

    /*IEnumerator test()
    {
        yield return new WaitForSeconds(5);

        movement.Input = new PacmanInput();
        movement.Input.OnEnable();

        yield return new WaitForSeconds(5);

        movement.Input = new AiInput(this);
        movement.Input.OnEnable();
    }*/

}
