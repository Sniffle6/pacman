﻿
using UnityEngine;

public class Pinky : Ghost
{

    public override Vector2 GetChaseTarget()
    {
        return (Vector2)PacmanTransform.position + (PacmanMovement.DirectionMovingIn*4);
    }

}

