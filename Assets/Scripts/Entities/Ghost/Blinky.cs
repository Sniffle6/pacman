﻿
using UnityEngine;

public class Blinky : Ghost
{

    public override Vector2 GetChaseTarget()
    {
        return PacmanTransform.position;
    }
}
