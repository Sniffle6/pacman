﻿
using UnityEngine;

public class Clyde : Ghost
{

    protected Transform _transform;

    protected override void Awake()
    {
        base.Awake();
        _transform = transform;
    }

    public override Vector2 GetChaseTarget()
    {
        if (Vector2.Distance(_transform.position, PacmanTransform.position) < 8)
        {
            return ScatterTarget;
        }
        else
        {
            return PacmanTransform.position;
        }
    }

}

