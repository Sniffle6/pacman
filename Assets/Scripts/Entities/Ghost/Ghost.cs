﻿using UnityEngine;

public abstract class Ghost : Entity
{
    [SerializeField]
    private Vector2 _scatterTarget;
    [SerializeField]
    private Vector2 _freightenedTarget;

    public Movement PacmanMovement { get; set; }
    public Transform PacmanTransform { get; set; }

    public Vector2 ScatterTarget { get { return _scatterTarget; } private set { } }
    public Vector2 FreightenedTarget { get { return _freightenedTarget; } private set { } }



    public abstract Vector2 GetChaseTarget();

    protected override void Awake()
    {
        movement = GetComponent<Movement>();
    }



}

