﻿
using UnityEngine;

public class Inky : Ghost
{
    [SerializeField]
    private Transform _blinky;



    public override Vector2 GetChaseTarget()
    {
        var angle = ((Vector2)PacmanTransform.position + (PacmanMovement.DirectionMovingIn * 2)) - (Vector2)_blinky.position;
        return (Vector2)PacmanTransform.position + angle;
    }


}

