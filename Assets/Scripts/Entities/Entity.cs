﻿using UnityEngine;

public abstract class Entity : MonoBehaviour
{
    [SerializeField]
    private Vector2 _target;
    public Vector2 Target { get { return _target; } set { _target = value; } }

    [HideInInspector]
    public Movement movement;

    protected virtual void Awake()
    {
        movement = GetComponent<Movement>();
    }



}

