﻿using System.Collections;

[System.Serializable]
public abstract class State
{
    protected GhostManager _manager;

    public State(GhostManager manager)
    {
        _manager = manager;
    }

    public virtual IEnumerator Start()
    {

        yield break;
    }

}

