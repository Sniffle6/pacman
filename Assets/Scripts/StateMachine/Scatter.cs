﻿using System.Collections;

public class Scatter : State
{

    public Scatter(GhostManager manager) : base(manager)
    {
    }

    public override IEnumerator Start()
    {
        var _ghost = _manager.ghost;
        for (int i = 0; i < _manager.ghost.Length; i++)
        {
            _ghost[i].Target = _ghost[i].ScatterTarget;
        }
        yield break;
    }
}

