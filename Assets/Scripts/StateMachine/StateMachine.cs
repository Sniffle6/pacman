﻿using UnityEngine;

public abstract class StateMachine : MonoBehaviour
{
    protected State State;
    protected State PreviousState;
    public State GetState { get { return this.State; } private set { } }

    [SerializeField]
    protected float _timeInState;

    public void SetState(State state)
    {
        PreviousState = this.State;
        this.State = state;
        _timeInState = 0;
        StartCoroutine(this.State.Start());
    }

}

