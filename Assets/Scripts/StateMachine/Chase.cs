﻿using System.Collections;
using UnityEngine;

public class Chase : State
{
    public Chase(GhostManager manager) : base(manager)
    {
    }

    public override IEnumerator Start()
    {
        while (_manager.GetState == this)
        {
            var _ghost = _manager.ghost;
            for (int i = 0; i < _manager.ghost.Length; i++)
            {
                _ghost[i].Target = _ghost[i].GetChaseTarget();
            }
            yield return new WaitForEndOfFrame();
        }
    }


}

