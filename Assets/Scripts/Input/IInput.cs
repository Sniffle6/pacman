﻿using UnityEngine;

public interface IInput
{

    Vector2 Axis { get; set; }


    void OnEnable();
    void OnDisable();

}