﻿using UnityEngine;

public interface IHaveInput
{
    Entity Parent { get; set; }

    Vector2 Axis { get; set; }

    void Initiate(Entity parent);

    void OnEnable();
    void OnDisable();

}