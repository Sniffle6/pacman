﻿using System;
using UnityEngine;
using UnityEngine.InputSystem;

public class PacmanInput : IInput
{
    public PlayerInputActions InputActions { get; set; }

    [SerializeField]
    protected Vector2 _axis;
    public Vector2 Axis { get { return _axis; } set { _axis = value; } }

    public PacmanInput()
    {
        InputActions = new PlayerInputActions();
    }



    private void UpdateAxisWithInput(InputAction.CallbackContext context)
    {
        var value = context.ReadValue<Vector2>();

        //bad code for checking if pressing x movement and y movement key
        if (Math.Abs(value.x) >= 0.1f && Math.Abs(value.y) >= 0.1f)
        {
            value = new Vector2(0, 1);//just setting x to 0 and y to 1
        }//help

        _axis = value != Vector2.zero ? value : Axis;
    }



    public void OnEnable()
    {
        InputActions.Pacman.Move.performed += UpdateAxisWithInput;
        InputActions.Enable();
    }

    public void OnDisable()
    {
        InputActions.Pacman.Move.performed -= UpdateAxisWithInput;
        InputActions.Disable();
    }

}

