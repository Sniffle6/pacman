﻿using UnityEngine;

public class AiInput : IInput
{
    [SerializeField]
    protected Vector2 _axis;
    public Vector2 Axis { get { return _axis; } set { _axis = value; } }

    public Entity Parent { get; set; }
    private Transform _parentPosition;

    private Movement _movement;

    private Vector2 _target;
    private Vector2 _directionToGo = Vector2.zero;

    private float _closestPoint = 1000;

    private Vector2[] possibleDirections = new Vector2[] {
        Vector2.up,
        Vector2.down,
        Vector2.left,
        Vector2.right
    };


    public AiInput(Entity parent)
    {
        Parent = parent;
        _parentPosition = parent.transform;
        _movement = Parent.GetComponent<Movement>();
    }


    private void Decide(Vector2 direction)
    {
        _target = Parent.Target;

        _closestPoint = 1000;

        for (int i = 0; i < possibleDirections.Length; i++)
        {
            if (possibleDirections[i] == -_axis)
            {
                continue;
            }

            if (_movement.GetCellCollision((Vector2)_parentPosition.position + possibleDirections[i]))
            {
                continue;
            }

            float distance = Vector2.Distance((Vector2)_parentPosition.position + possibleDirections[i], _target);
            if (distance < _closestPoint)
            {
                _closestPoint = distance;
                _directionToGo = possibleDirections[i];
            }
        }
        _axis = _directionToGo;
    }


    public void OnEnable()
    {
        _movement.onBeforeSetDirection += Decide;
    }

    public void OnDisable()
    {
        _movement.onBeforeSetDirection -= Decide;
    }
}