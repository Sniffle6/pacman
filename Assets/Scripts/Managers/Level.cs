﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Level Data", menuName = "Level Data")]
public class Level : ScriptableObject
{
    public LevelData[] Levels; 
}
