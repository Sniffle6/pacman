﻿using System.Collections;
using UnityEngine;
using UnityEngine.Tilemaps;

public class GhostManager : StateMachine
{

    public Ghost[] ghost;


    [SerializeField]
    private Movement _pacmanMovement;

    [SerializeField]
    private int currentLevel;

    [SerializeField]
    private Level levels;


    private WaitForSeconds _waitForOneSecond = new WaitForSeconds(1);
    private Coroutine c_DetermineState;
    private bool _determining;


    void Start()
    {
        Camera.main.eventMask = 0;

        for (int i = 0; i < ghost.Length; i++)
        {
            ghost[i].PacmanMovement = _pacmanMovement;
            ghost[i].PacmanTransform = _pacmanMovement.transform;
        }
        SetState(new Scatter(this));

        _determining = true;
        c_DetermineState = StartCoroutine(DetermineState());
    }

    IEnumerator DetermineState()
    {
        while (_determining)
        {
            _timeInState += 1;
            if (State.GetType().Equals(typeof(Scatter)))
            {
                if (_timeInState > levels.Levels[currentLevel].ScatterTime)
                {
                    currentLevel++;
                    if (currentLevel > levels.Levels.Length)
                        currentLevel = levels.Levels.Length;
                    SetState(new Chase(this));
                    FlipDirection();
                }
            }
            else if (State.GetType().Equals(typeof(Chase)))
            {
                if (_timeInState > levels.Levels[currentLevel].ChaseTime)
                {
                    currentLevel++;
                    if (currentLevel > levels.Levels.Length)
                        currentLevel = levels.Levels.Length;
                    SetState(new Scatter(this));
                    FlipDirection();
                }
            }

            yield return _waitForOneSecond;
        }
    }
    private void FlipDirection()
    {
        for (int i = 0; i < ghost.Length; i++)
        {
            ghost[i].movement.Input.Axis = -ghost[i].movement.Input.Axis;
        }
    }
}
