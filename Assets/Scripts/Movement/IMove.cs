﻿using System.Collections;

public interface IMove
{
    Movement Parent { get; set; }

    void InitiateMove();
    IEnumerator Move();
}