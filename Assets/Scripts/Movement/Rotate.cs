﻿using UnityEngine;

public class Rotate : MonoBehaviour
{
    private Movement _movement;
    private Transform _transform;

    void Awake()
    {
        _movement = GetComponent<Movement>();

        _transform = transform;
    }

    private void RotateEntity(Vector2 direction)
    {
        if (direction.y >= 1)
        {
            _transform.eulerAngles = new Vector3(0, 0, 90);
        }
        else if (direction.y <= -1)
        {
            _transform.eulerAngles = new Vector3(0, 0, -90);
        }
        if (direction.x >= 1)
        {
            _transform.eulerAngles = new Vector3(0, 0, 0);
        }
        else if (direction.x <= -1)
        {
            _transform.eulerAngles = new Vector3(0, 0, 180);
        }
    }

    private void OnEnable()
    {
        _movement.onAfterSetDirection += RotateEntity;
    }

    private void OnDisable()
    {
        _movement.onAfterSetDirection -= RotateEntity;
    }
}
