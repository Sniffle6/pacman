﻿using System.Collections;
using UnityEngine;

public class InstantMove : IMove
{
    protected Vector2 _directionMovingInLastFrame = Vector2.one;

    public Movement Parent { get; set; }

    protected readonly WaitForEndOfFrame _wait = new WaitForEndOfFrame();

    protected Lerp _lerp = new Lerp();

    public InstantMove(Movement movement)
    {
        Parent = movement;
    }

    public void InitiateMove()
    {
        Parent.onBeforeSetDirection?.Invoke(Parent.DirectionMovingIn);

        Parent.SetMovementDirection();

        Parent.onAfterSetDirection?.Invoke(Parent.DirectionMovingIn);
    }

    public IEnumerator Move()
    {
        while (Parent.Moving)
        {

            InitiateMove();

            if (Parent.DirectionMovingIn != _directionMovingInLastFrame || _lerp.IsComplete())
            {
                if (!Parent.GetCellCollision((Vector2)Parent.Transform.position + Parent.DirectionMovingIn))
                    _lerp.InitiateLerp(Parent.GetCellPosition(Parent.Transform.position), Parent.GetCellPosition((Vector2)Parent.Transform.position + Parent.DirectionMovingIn));
            }
            _directionMovingInLastFrame = Parent.DirectionMovingIn;

            Parent.Transform.position = Vector2.Lerp(_lerp.StartPos, _lerp.EndPos, _lerp.Percent);

            _lerp.IterateLerp(Parent.Speed);

            yield return _wait;
        }
    }
}
