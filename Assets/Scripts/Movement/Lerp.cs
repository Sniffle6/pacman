﻿using UnityEngine;

public class Lerp
{
    public Vector2 StartPos { get; private set; }
    public Vector2 EndPos { get; private set; }

    public float CurrentLerpTime { get; private set; }
    public float LerpTime { get; private set; } = 1f;

    public float Percent => CurrentLerpTime / LerpTime;


    public void InitiateLerp(Vector2 start, Vector2 end)
    {
        StartPos = start;
        EndPos = end;
        CurrentLerpTime = 0;
    }

    public void IterateLerp(float speed)
    {
        CurrentLerpTime += Time.deltaTime * speed;
    }
    public bool IsComplete()
    {
        return CurrentLerpTime > LerpTime;
    }

}