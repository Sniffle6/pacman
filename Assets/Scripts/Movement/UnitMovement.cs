﻿using System.Collections;
using UnityEngine;

public class UnitMovement : IMove
{

    public Movement Parent { get; set; }

    protected readonly WaitForEndOfFrame _wait = new WaitForEndOfFrame();

    protected Lerp _lerp = new Lerp();

    public UnitMovement(Movement movement)
    {
        Parent = movement;
    }

    public void InitiateMove()
    {

        Parent.onBeforeSetDirection?.Invoke(Parent.DirectionMovingIn);

        Parent.SetMovementDirection();

        Parent.onAfterSetDirection?.Invoke(Parent.DirectionMovingIn);


        if (Parent.GetCellCollision((Vector2)Parent.Transform.position + Parent.DirectionMovingIn))
            return;


        _lerp.InitiateLerp(Parent.GetCellPosition(Parent.Transform.position), Parent.GetCellPosition((Vector2)Parent.Transform.position + Parent.DirectionMovingIn));

    }

    public IEnumerator Move()
    {
        while (Parent.Moving)
        {

            _lerp.IterateLerp(Parent.Speed);
            if (_lerp.IsComplete())
                InitiateMove();

            Parent.Transform.position = Vector2.Lerp(_lerp.StartPos, _lerp.EndPos, _lerp.Percent);

            yield return _wait;
        }
    }
}