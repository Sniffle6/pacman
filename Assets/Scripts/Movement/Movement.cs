﻿using System.Collections;
using UnityEngine;
using UnityEngine.Tilemaps;

public delegate void IterateMoveCallback(Vector2 _direction);

public class Movement : MonoBehaviour
{
    [SerializeField]
    protected Tilemap _tilemap;

    [SerializeField]
    protected float _speed;
    public float Speed => _speed;

    [SerializeField]
    private Vector2 _directionMovingIn;
    public Vector2 DirectionMovingIn => _directionMovingIn;
    
    public IInput Input;
    public IMove mover;
    
    private bool _moving;
    public bool Moving => _moving;

    protected Transform _transform;
    public Transform Transform => _transform;


    public IterateMoveCallback onBeforeSetDirection;
    public IterateMoveCallback onAfterSetDirection;
    

    private void Awake()
    {
        _transform = transform;

        Input = new AiInput(GetComponent<Entity>());
    }

    private void Start()
    {
        mover = new UnitMovement(this);
        mover.InitiateMove();

        _moving = true;
        StartCoroutine(mover.Move());
    }

    public void SetMovementDirection()
    {
        if (!GetCellCollision((Vector2)_transform.position + Input.Axis))
        {
            _directionMovingIn = Input.Axis;
        }
    }

    public Vector2 GetCellPosition(Vector2 position)
    {
        return _tilemap.GetCellCenterWorld(_tilemap.WorldToCell(position));
    }

    public bool GetCellCollision(Vector2 position)
    {
        return _tilemap.GetTile(Vector3Int.FloorToInt(position));
    }


    private void OnEnable()
    {
        Input.OnEnable();
    }

    private void OnDisable()
    {
        Input.OnDisable();
    }

}
